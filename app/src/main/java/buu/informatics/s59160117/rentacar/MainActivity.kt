package buu.informatics.s59160117.rentacar

import android.databinding.DataBindingUtil
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import buu.informatics.s59160117.rentacar.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private  lateinit var binding: ActivityMainBinding
    //เอาไว้ดูว่าที่จอดอันไหน
    var idSlot:Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        // Set ค่าต่างๆ
        Initialized()
        // เปิดแอฟมาให้โผล่มาแค่ Slot1
        selectionSlot1()

        binding.apply {
            buttonSlot1.setBackgroundColor(Color.GREEN)
            buttonSlot2.setBackgroundColor(Color.GREEN)
            buttonSlot3.setBackgroundColor(Color.GREEN)


            buttonSlot1.setOnClickListener{
                idSlot = 1
               selectionSlot1()
            }

            buttonSlot2.setOnClickListener{
                idSlot = 2
                selectionSlot2()
            }

            buttonSlot3.setOnClickListener{
                idSlot = 3
                selectionSlot3()
            }

            buttonUpdate.setOnClickListener{
                addCar(it,idSlot)
            }

            buttonDelete.setOnClickListener{
                removeCar(it,idSlot)
            }
        }

    }

    private fun Initialized() {
        binding.apply {
            val car: Car = Car("","","")
            val car2: Car2 = Car2("","","")
            val car3: Car3 = Car3("","","")
            binding.car = car
            binding.car2 = car2
            binding.car3 = car3

        }
    }

    private fun selectionSlot1() {

        binding.apply {
            editTextBrand.visibility = View.VISIBLE
            editTextLicense.visibility = View.VISIBLE
            editTextName.visibility = View.VISIBLE

            editTextBrand2.visibility = View.GONE
            editTextLicense2.visibility = View.GONE
            editTextName2.visibility = View.GONE

            editTextBrand3.visibility = View.GONE
            editTextLicense3.visibility = View.GONE
            editTextName3.visibility = View.GONE
        }
    }

    private fun selectionSlot2() {

        binding.apply {
            editTextBrand.visibility = View.GONE
            editTextLicense.visibility = View.GONE
            editTextName.visibility = View.GONE

            editTextBrand2.visibility = View.VISIBLE
            editTextLicense2.visibility = View.VISIBLE
            editTextName2.visibility = View.VISIBLE

            editTextBrand3.visibility = View.GONE
            editTextLicense3.visibility = View.GONE
            editTextName3.visibility = View.GONE
        }
    }

    private fun selectionSlot3() {

        binding.apply {

            editTextBrand.visibility = View.GONE
            editTextLicense.visibility = View.GONE
            editTextName.visibility = View.GONE

            editTextBrand2.visibility = View.GONE
            editTextLicense2.visibility = View.GONE
            editTextName2.visibility = View.GONE

            editTextBrand3.visibility = View.VISIBLE
            editTextLicense3.visibility = View.VISIBLE
            editTextName3.visibility = View.VISIBLE
        }
    }

    private fun addCar(view: View,int: Int) {
        binding.apply {

            if (int == 1) {
                car?.License = editTextLicense.text.toString()
                car?.Brand = editTextBrand.text.toString()
                car?.Name = editTextName.text.toString()
                invalidateAll()
                buttonSlot1.setBackgroundColor(Color.RED)
            }else if (int == 2) {
                car2?.License2 = editTextLicense2.text.toString()
                car2?.Brand2 = editTextBrand2.text.toString()
                car2?.Name2 = editTextName2.text.toString()
                invalidateAll()
                buttonSlot2.setBackgroundColor(Color.RED)
            }else if (int == 3) {
                car3?.License3 = editTextLicense3.text.toString()
                car3?.Brand3 = editTextBrand3.text.toString()
                car3?.Name3 = editTextName3.text.toString()
                invalidateAll()
                buttonSlot3.setBackgroundColor(Color.RED)
            }
        }
    }

    private fun removeCar(view: View,int: Int) {

        binding.apply {
            if (int == 1) {
                editText_license.text.clear()
                editText_brand.text.clear()
                editText_name.text.clear()
                invalidateAll()
                buttonSlot1.setBackgroundColor(Color.GREEN)
            }else if (int == 2) {
                editText_license2.text.clear()
                editText_brand2.text.clear()
                editText_name2.text.clear()
                invalidateAll()
                buttonSlot2.setBackgroundColor(Color.GREEN)
            }else if (int == 3) {
                editText_license3.text.clear()
                editText_brand3.text.clear()
                editText_name3.text.clear()
                invalidateAll()
                buttonSlot3.setBackgroundColor(Color.GREEN)
            }

        }
    }
}
